# Prueba Remota Rootstack

> Proyecto test de Desarrollo para Rootsack (2019)

![Version](https://img.shields.io/badge/version-0.1-green.svg)
![Scope](https://img.shields.io/badge/Laravel-5.7-blue.svg)
![Scope](https://img.shields.io/badge/PHP-7.2-blue.svg)
![Status](https://img.shields.io/badge/status-success-green.svg)
![Scope](https://img.shields.io/badge/scope-private-red.svg)
![Dependencies](https://img.shields.io/badge/dependencies-ok-green.svg)

Laravel es un framework de aplicaci�n web con una sintaxis expresiva y elegante. Creemos que el desarrollo debe ser una experiencia agradable y creativa para ser verdaderamente satisfactoria. Laravel intenta eliminar el dolor del desarrollo facilitando las tareas comunes que se utilizan en la mayor�a de los proyectos web, como la autenticaci�n, el enrutamiento, las sesiones, la cola de espera y el almacenamiento en cach�.

Laravel es accesible, pero potente, proporcionando herramientas necesarias para aplicaciones grandes y robustas. Una magn�fica inversi�n del contenedor de control, sistema de migraci�n expresiva y soporte de pruebas unitarias estrechamente integradas le brindan las herramientas que necesita para construir cualquier aplicaci�n con la que est� encargado.

## Documentaci�n Oficial

Puede encontrar m�s informaci�n en  [Laravel website](http://laravel.com/docs).


## Licencia

Laravel es un software de c�digo abierto con licencia [MIT license](http://opensource.org/licenses/MIT).

## Introducci�n

Por favor lea la documentaci�n antes de bajar el repositorio y realizar cambios en el mismo.
A continuaci�n encontrar� instrucciones para configurar su equipo con el ambiente de trabajo necesario,
como as� tambi�n una descripci�n de las tareas m�s comunes.

Del mismo modo encontrar� una descripci�n de los requisitos de documentaci�n y formatos utilizados para
realizar commits, entre otras tareas.

## Setup Inicial

### En primer lugar debe crear una carpeta en donde alojar los archivos del proyecto, y clonar el repositorio

> Creamos una carpeta en donde se alojar�n todos los proyectos que deseamos est�n disponibles en el servidor Homestead.
> Dicha carpeta la llamaremos ~/rootstack

```
cd ~
mkdir rootstack
```

#### Clonamos el repositorio
Descargamos el c�digo del proyecto desde el repositorio.
No olvidarse reemplazar __USERNAME__ por su nombre de usuario de BitBucket.

```
cd ~/rootstack
git clone https://USERNAME@bitbucket.org/marshalejrt/testrootstack.git
cd ~/testrootstack/rootstackApp
```

Una vez finalizado este proceso tendr� el c�digo disponible en la carpeta `~/rootstack/testrootstack/rootstackApp`

### En segundo lugar debe seguir los siguientes pasos para configurar el proyecto

```
  	Configurar parametros de conexion en el archivo .env, crearlo si no existe
```
```
  	Crear la base de datos en el servidor
```
```
	Instalar Composer en la app  `composer install`
```
```
	Instalar Componentes npm en la app  `npm install`
```
```
	Compilar los archivos del front  `npm run dev or npm run production`
```  
```			
	Ejecutar la llave `php artisan key:generate`
```  
```			
	Opcional `composer dump-autoload`
```  
```			
	Ejecutar la Aplicaci�n con el siguiente comando `php artisan serve`
```  
```			
	acceder a la aplicaci�n desde: `http://localhost:8000` 
```  
