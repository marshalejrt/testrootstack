<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Rootstack</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        <!-- Styles -->

    </head>

    <>

    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">Rootstack</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item ">
                        <a class="nav-link" href="/">Home</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('fetchorder') }}">Fetch & Order </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('fetchfind') }}">Fetch & Find </a>
                    </li>
                    <li class="nav-item active" >
                        <a class="nav-link" href="{{ url('fetchcount') }}">Fetch & Count <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('fastedship') }}">Fastest ship</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('planetterrain') }}">Planet by Terrain</a>
                    </li>

                </ul>
                {{--<form class="form-inline mt-2 mt-md-0">--}}
                    {{--<input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">--}}
                    {{--<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>--}}
                {{--</form>--}}
            </div>
        </nav>
    </header>

    <main role="main">


        <hr class="featurette-divider">

       

        <div class="container marketing">
            <hr class="featurette-divider">
            <div class="row featurette">
                <div class="col-md-7">
                    <h2 class="featurette-heading">Fetch & Count <span class="text-muted">Obtener y Contar</span></h2>

                    <p class="lead">Usando https://randomuser.me/ como fuente de data, crear function, que retornara un
                        char/string:<br>
                        - Debera obtener 5 personas.<br>
                        - En base a los nombres debera calcular cual es la letra mas utilizada.<br>
                    </p>
                </div>
                <div class="col-md-5">
                    <p><a class="btn btn-lg btn-primary " href="#" name="btn_showUsers" id="btn_showUsers"
                          role="button">Encontrar</a> <strong class="d-inline-block mb-2 text-primary" style="display: none!important" id="buscando">Buscando Usuarios...</strong></p>

                </div>
            </div>

            <div class="container" id="result" name="result" style="display: none !important;">
                <div class="row">
                    <div class=" col align-self-start">
                        <div class="card flex-md-row mb-4 box-shadow h-md-250">
                            <div class="card-body d-flex flex-column align-items-start">
                                <strong class="d-inline-block mb-2 text-primary">La letra mas utilizada en el Nombre es:</strong>
                                <h3 class="mb-0"><a class="text-dark" id="char_used" name="char_used">A</a></h3>
                                <div class="mb-1 text-muted" id="count_used" name="count_used">Se uso: 35 Veces</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-2 users" name="users" id="users">
                {{--Listado de Usuarios--}}
            </div>





            <!-- /END THE FEATURETTES -->
            <hr class="featurette-divider">

        </div>
        <!-- /.container -->


        <!-- FOOTER -->
        <footer class="blog-footer container">
            <p>Creado por <a href="http://indexsoftwareca.com/ejdev/">Ing. Efrain Rodriguez</a> para <a
                        href="https://rootstack.com">Rootstack</a>.</p>

            <p>
                <a href="#">Ir al Inicio</a>
            </p>
        </footer>
    </main>




        <script src="{{asset('js/app.js')}}"></script>
    </body>
    <script>
        $("#btn_showUsers").click(function () {
            $('#buscando').show();
            //Creamos funcion para Capitalizar Nombres
            String.prototype.capitalize = function () {
                return this.replace(/(?:^|\s)\S/g, function (a) {
                    return a.toUpperCase();
                });
            };

            /*Declaramos las variables a utilizar*/
            var i;
            var max_count=0;
            var count=0;
            var count_user=0;
            var current_character='';
            var max_character='';
            var names ='';
            var names_order='';
            /*Creamos funcion sortAphabets para ordenar alfabeticamente los las letras de cada nombre
             para luego contar los caracteres        */
            var sortAlphabets = function (text) {
                return text.split('').sort().join('');
            };
            //Consultamos los datos
            $.ajax({
                url: 'https://randomuser.me/api/?nat=us&results=5',
                dataType: 'json',
                success: function (data) {
                    //Mostramos en consola los datos obtenidos
                    console.log('Resultados Fetch & count ');
                    console.log(data['results']);
                    //Limpiamos la lista de nombres
                    $('#buscando').attr('style','display:none !important');
                    $('#users').html('');
                    count_user=0;
                    //Recorremos los usuarios obtenidos para mostrarlos en el front
                    data['results'].forEach(function (user) {
                        //Acumulamos los nombres
                        names+=user['name']['first'];
                        count_user++;
                        $('#users').append(
                                $('<div>')
                                        .attr('class', 'col-md-6')
                                        .append(
                                        $('<div>')
                                                .attr('class', 'card flex-md-row mb-4 box-shadow h-md-250')
                                                .append(
                                                $('<div>')
                                                        .attr('class', 'card-body d-flex flex-column align-items-start')
                                                        .append(
                                                        $('<strong>')
                                                                .attr('class', 'd-inline-block mb-2 text-primary')
                                                                .append(count_user)
                                                )
                                                        .append(
                                                        $('<h3>')
                                                                .attr('class', 'mb-0')
                                                                .append(
                                                                $('<a>')
                                                                        .attr('class', 'text-dark')
                                                                        .append(user['name']['first'].capitalize() + ' ' + user['name']['last'].capitalize())
                                                        )
                                                )
                                                        .append(
                                                        $('<p>')
                                                                .attr('class', 'card-text mb-auto')
                                                                .append('Direccion: ' + user['location']['street'] + ' ' + user['location']['city'] + ' ' + user['location']['state'])
                                                )
                                                        .append(
                                                        $('<div>')
                                                                .attr('class', 'mb-1 text-muted')
                                                                .append('Edad: ' + user['dob']['age'] )
                                                )
                                                        .append(
                                                        $('<strong>')
                                                                .attr('class', 'd-inline-block mb-2 text-primary')
                                                                .append(user['email'])
                                                )
                                        )
                                                .append(
                                                $('<img>')
                                                        .attr('class', 'card-img-right flex-auto d-none d-md-block')
                                                        .attr('style', 'width: auto; height: 200px;')
                                                        .attr('data-holder-rendered', 'true')
                                                        .attr('src', user['picture']['large'])
                                        )
                                )
                        )

                    });

                    //Ordenamos los caracteres de todos los nombres
                    names_order=sortAlphabets(names);
                    //Establecemos el primer caracter
                    current_character=names_order.substr(0,1);
                    //Recorremos los caracteres de todos los nombres para contarlos
                    for (i=0;i<names_order.length;i++){
                        //Si es diferente establecemos el m�s utilizado
                        if(current_character!==names_order.substr(i,1)){
                            if(count>=max_count){
                                max_count=count;
                                //Guardamos el caracter m�s utilizado
                                max_character=names_order.substr(i-1,1);
                            }
                            //Inicializamos el contador
                            count=0;
                        }
                        //Contamos
                        count++;
                        //Establecemos el caracter actual para comparar
                        current_character=names_order.substr(i,1);
                    }
                    //Mostramos en consola el Caracter mas utilizado
                    console.log('Caracter m�s utilizado');
                    console.log(max_character);
                    $('#result').show(300);
                    $('#char_used').html(max_character);
                    //Mostramos en consola el total de veces utilizado
                    console.log('Cantidad utilizada');
                    console.log(max_count);
                    $('#count_used').html('Se uso '+max_count+ ' veces');

                }
            });

        });

    </script>

</html>
