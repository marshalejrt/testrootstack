<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Rootstack</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        <!-- Styles -->

    </head>

    <>

    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">Rootstack</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item ">
                        <a class="nav-link" href="/">Home</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('fetchorder') }}">Fetch & Order </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('fetchfind') }}">Fetch & Find <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('fetchcount') }}">Fetch & Count</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('fastedship') }}">Fastest ship</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('planetterrain') }}">Planet by Terrain</a>
                    </li>

                </ul>
                {{--<form class="form-inline mt-2 mt-md-0">--}}
                    {{--<input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">--}}
                    {{--<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>--}}
                {{--</form>--}}
            </div>
        </nav>
    </header>

    <main role="main">


        <hr class="featurette-divider">



        <div class="container marketing">
            <hr class="featurette-divider">
            <div class="row featurette">
                <div class="col-md-7">
                    <h2 class="featurette-heading">Fetch & Find <span class="text-muted">Obtener y Encontrar</span></h2>

                    <p class="lead">Usando https://randomuser.me/ como fuente de data, crear function, que acepte como
                        argumento una edad y retorne la data de la persona:<br>
                        - Retornara 1 sola persona.<br>
                        - La persona a retornar sera mayor de la edad especificada como argumento.</p>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="Edad">Edad:</label>
                        <input type="number" min="1" class="form-control" id="Edad" name="Edad" placeholder="Edad">
                    </div>
                    <p><a class="btn btn-lg btn-primary " href="#" name="btn_showUsers" id="btn_showUsers"
                          role="button">Encontrar</a> <strong class="d-inline-block mb-2 text-primary" style="display: none!important" id="buscando">Buscando Usuarios...</strong></p>

                </div>
            </div>

            <div class="row mb-2 users" name="users" id="users">
                {{--Listado de Usuarios--}}
            </div>


            <!-- /END THE FEATURETTES -->
            <hr class="featurette-divider">

        </div>
        <!-- /.container -->


        <!-- FOOTER -->
        <footer class="blog-footer container">
            <p>Creado por <a href="http://indexsoftwareca.com/ejdev/">Ing. Efrain Rodriguez</a> para <a
                        href="https://rootstack.com">Rootstack</a>.</p>

            <p>
                <a href="#">Ir al Inicio</a>
            </p>
        </footer>
    </main>




        <script src="{{asset('js/app.js')}}"></script>
    </body>
    <script>
        $("#btn_showUsers").click(function () {
            if($('#Edad').val()<1){
                alert('Debe ingresar una edad, intente de nuevo');
                return;
            }
            $('#buscando').show();
            /*Declaramos las variables a utilizar*/
            var user=[];
            var age=$('#Edad').val();
            //Creamos funcion para Capitalizar Nombres
            String.prototype.capitalize = function () {
                return this.replace(/(?:^|\s)\S/g, function (a) {
                    return a.toUpperCase();
                });
            };
            $.ajax({
                url: 'https://randomuser.me/api/?nat=us&results=2000',
                dataType: 'json',
                success: function(data) {
                    console.log('Resultados Fetch & find');
                    console.log(data['results']);
                    //Recorremos el resultado para guardar la lista de nombres y su posicion
                    try {
                        data['results'].forEach(function (item){
                            //Guardamos en un arreglo el Nombre de usuario y su posicion
                            if(item['dob']['age']>age){
                                user=item;
                                throw "break";
                            }
                        });
                    } catch (e) { }
                    //Mostramos resultado obtenido en consola
                    console.log('Resultado con Edad Mayor a ' +age);
                    console.log(user);
                    //Limpiamos la lista de nombres
                    $('#buscando').attr('style','display:none !important');
                    $('#users').html('');
                    $('#users').append(
                            $('<div>')
                                    .attr('class', 'col-md-6')
                                    .append(
                                    $('<div>')
                                            .attr('class', 'card flex-md-row mb-4 box-shadow h-md-250')
                                            .append(
                                            $('<div>')
                                                    .attr('class', 'card-body d-flex flex-column align-items-start')
                                                    .append(
                                                    $('<strong>')
                                                            .attr('class', 'd-inline-block mb-2 text-primary')
                                                            .append(1)
                                            )
                                                    .append(
                                                    $('<h3>')
                                                            .attr('class', 'mb-0')
                                                            .append(
                                                            $('<a>')
                                                                    .attr('class', 'text-dark')
                                                                    .append(user['name']['first'].capitalize() + ' ' + user['name']['last'].capitalize())
                                                    )
                                            )
                                                    .append(
                                                    $('<p>')
                                                            .attr('class', 'card-text mb-auto')
                                                            .append('Direccion: ' + user['location']['street'] + ' ' + user['location']['city'] + ' ' + user['location']['state'])
                                            )
                                                    .append(
                                                    $('<div>')
                                                            .attr('class', 'mb-1 text-muted')
                                                            .append('Edad: ' + user['dob']['age'] )
                                            )
                                                    .append(
                                                    $('<strong>')
                                                            .attr('class', 'd-inline-block mb-2 text-primary')
                                                            .append(user['email'])
                                            )
                                    )
                                            .append(
                                            $('<img>')
                                                    .attr('class', 'card-img-right flex-auto d-none d-md-block')
                                                    .attr('style', 'width: auto; height: 200px;')
                                                    .attr('data-holder-rendered', 'true')
                                                    .attr('src', user['picture']['large'])
                                    )
                            )
                    )

                }
            });
        });

    </script>

</html>
