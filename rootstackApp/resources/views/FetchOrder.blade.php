<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Rootstack</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        <!-- Styles -->

    </head>

    <>

    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">Rootstack</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item ">
                        <a class="nav-link" href="/">Home</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('fetchorder') }}">Fetch & Order <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('fetchfind') }}">Fetch & Find</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('fetchcount') }}">Fetch & Count</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('fastedship') }}">Fastest ship</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('planetterrain') }}">Planet by Terrain</a>
                    </li>

                </ul>
                {{--<form class="form-inline mt-2 mt-md-0">--}}
                    {{--<input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">--}}
                    {{--<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>--}}
                {{--</form>--}}
            </div>
        </nav>
    </header>

    <main role="main">


        <hr class="featurette-divider">



        <div class="container marketing">
            <hr class="featurette-divider">
            <div class="row featurette">
                <div class="col-md-7">
                    <h2 class="featurette-heading">Fetch & Order <span class="text-muted">Obtener y ordenar</span></h2>

                    <p class="lead">Usando https://randomuser.me/ como fuente de data, crear una funcion que retorne un
                        arreglo:<br>
                        - Debe retornar 10 personas.<br>
                        - Las personas deberan estar ordenadas por nombre</p>
                </div>
                <div class="col-md-5">
                    <p><a class="btn btn-lg btn-primary " href="#" name="btn_showUsers" id="btn_showUsers"
                          role="button">Procesar</a><strong class="d-inline-block mb-2 text-primary" style="display: none!important" id="buscando">Buscando Usuarios...</strong></p>
                </div>
            </div>

            <div class="row mb-2 users" name="users" id="users">
                {{--Listado de Usuarios--}}
            </div>


            <!-- /END THE FEATURETTES -->
            <hr class="featurette-divider">

        </div>
        <!-- /.container -->


        <!-- FOOTER -->
        <footer class="blog-footer container">
            <p>Creado por <a href="http://indexsoftwareca.com/ejdev/">Ing. Efrain Rodriguez</a> para <a
                        href="https://rootstack.com">Rootstack</a>.</p>

            <p>
                <a href="#">Ir al Inicio</a>
            </p>
        </footer>
    </main>




        <script src="{{asset('js/app.js')}}"></script>
    </body>
    <script>
        $("#btn_showUsers").click(function () {
            $('#buscando').show();
            /*Declaramos las variables a utilizar*/
            var items = [];
            var items_name = [];
            var items_order = [];
            var count = 0;
            //Creamos funcion para Capitalizar Nombres
            String.prototype.capitalize = function () {
                return this.replace(/(?:^|\s)\S/g, function (a) {
                    return a.toUpperCase();
                });
            };
            //Consultamos los 10 Usuarios
            $.ajax({
                url: 'https://randomuser.me/api/?nat=us&results=10',
                dataType: 'json',
                success: function (data) {
                    //Mostramos resultados en consola
                    console.log('Resultado sin ordenar por nombre Fetch & order');
                    console.log(data['results']);
                    //Recorremos el resultado para guardar la lista de nombres y su posicion
                    data['results'].forEach(function (item) {
                        //Guardamos en un arreglo el Nombre de usuario y su posicion
                        items_name.push({name: item['name']['first'], value: count});
                        count++;
                    });

                    //Ordenamos el arreglo con los nombres
                    items_name.sort(function (a, b) {
                        if (a.name > b.name) {
                            return 1;
                        }
                        if (a.name < b.name) {
                            return -1;
                        }
                        // a must be equal to b
                        return 0;
                    });

                    //Recorremos el arreglo ordenado para crear el arreglo completo con toda la
                    //información del usuario segun su posición
                    items_name.forEach(function (element) {
                        //Llenamos el arreglo de los usuarios segun el orden establecido
                        items_order.push(data['results'][element['value']]);
                    });
                    //Mostramos resultado ordenado en consola
                    console.log('Resultado ordenado por nombre Alfabeticamente');
                    console.log(items_order);
                    //Limpiamos la lista de nombres
                    $('#buscando').attr('style','display:none !important');
                    $('#users').html('');
                    //Iniciamos el conteo de usuarios
                    count = 0;
                    //Recorremos el array de usuarios para llenar la lista en el front
                    items_order.forEach(function (user) {
                        count++;
                        $('#users').append(
                                $('<div>')
                                        .attr('class', 'col-md-6')
                                        .append(
                                        $('<div>')
                                                .attr('class', 'card flex-md-row mb-4 box-shadow h-md-250')
                                                .append(
                                                $('<div>')
                                                        .attr('class', 'card-body d-flex flex-column align-items-start')
                                                        .append(
                                                        $('<strong>')
                                                                .attr('class', 'd-inline-block mb-2 text-primary')
                                                                .append(count)
                                                )
                                                        .append(
                                                        $('<h3>')
                                                                .attr('class', 'mb-0')
                                                                .append(
                                                                $('<a>')
                                                                        .attr('class', 'text-dark')
                                                                        .append(user['name']['first'].capitalize() + ' ' + user['name']['last'].capitalize())
                                                        )
                                                )
                                                        .append(
                                                        $('<p>')
                                                                .attr('class', 'card-text mb-auto')
                                                                .append('Direccion: ' + user['location']['street'] + ' ' + user['location']['city'] + ' ' + user['location']['state'])
                                                )
                                                        .append(
                                                        $('<div>')
                                                                .attr('class', 'mb-1 text-muted')
                                                                .append('Edad: ' + user['dob']['age'] )
                                                )
                                                        .append(
                                                        $('<strong>')
                                                                .attr('class', 'd-inline-block mb-2 text-primary')
                                                                .append(user['email'])
                                                )
                                        )
                                                .append(
                                                $('<img>')
                                                        .attr('class', 'card-img-right flex-auto d-none d-md-block')
                                                        .attr('style', 'width: auto; height: 200px;')
                                                        .attr('data-holder-rendered', 'true')
                                                        .attr('src', user['picture']['large'])
                                        )
                                )
                        )
                    });
                }
            });
        });


    </script>

</html>
