<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Rootstack</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        <!-- Styles -->

    </head>

    <>

    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">Rootstack</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item ">
                        <a class="nav-link" href="/">Home</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('fetchorder') }}">Fetch & Order </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('fetchfind') }}">Fetch & Find </a>
                    </li>
                    <li class="nav-item" >
                        <a class="nav-link" href="{{ url('fetchcount') }}">Fetch & Count </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('fastedship') }}">Fastest Ship </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('planetterrain') }}">Planet by Terrain <span class="sr-only">(current)</span></a>
                    </li>

                </ul>
                {{--<form class="form-inline mt-2 mt-md-0">--}}
                    {{--<input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">--}}
                    {{--<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>--}}
                {{--</form>--}}
            </div>
        </nav>
    </header>

    <main role="main">


        <hr class="featurette-divider">

       

        <div class="container marketing">
            <hr class="featurette-divider">
            <div class="row featurette">
                <div class="col-md-7">
                    <h2 class="featurette-heading">Planet by Terrain <span class="text-muted">Terrenos Planetarios</span></h2>

                    <p class="lead">Usando https://swapi.co/ como fuente de data, crear function, que retornara un string:<br>
                        - Aceptara un argumento tipo string, que indicara el tipo de terreno.<br>
                        - Retornara el nombre del planeta que coincida con los siguientes parametros:<br>
                        - Coincide el tipo de terreno especificado como parametro.<br>
                        - Si mas de un planeta coincide con dichos parametros, debera retornar el que posea mas<br>
                        poblacion.<br>

                    </p>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="Terreno">Terreno:</label>
                        <input class="form-control" id="Terreno" name="Terreno" placeholder="rainforests, rivers, mountains,jungles, forests, deserts, etc">
                    </div>
                    <p><a class="btn btn-lg btn-primary " href="#" name="btn_showUsers" id="btn_showUsers"
                          role="button">Encontrar</a> <strong class="d-inline-block mb-2 text-primary" style="display: none!important" id="buscando">Buscando Planetas...</strong></p>

                </div>
            </div>

            <div class="container" id="result" name="result" style="display: none !important;">
                <div class="row">
                    <div class=" col align-self-start">
                        <div class="card flex-md-row mb-4 box-shadow h-md-250">
                            <div class="card-body d-flex flex-column align-items-start">
                                <strong class="d-inline-block mb-2 text-primary">La letra mas utilizada en el Nombre es:</strong>
                                <h3 class="mb-0"><a class="text-dark" id="char_used" name="char_used">A</a></h3>
                                <div class="mb-1 text-muted" id="count_used" name="count_used">Se uso: 35 Veces</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-2 ships" name="ships" id="ships">
                {{--Listado de Usuarios--}}
            </div>





            <!-- /END THE FEATURETTES -->
            <hr class="featurette-divider">

        </div>
        <!-- /.container -->


        <!-- FOOTER -->
        <footer class="blog-footer container">
            <p>Creado por <a href="http://indexsoftwareca.com/ejdev/">Ing. Efrain Rodriguez</a> para <a
                        href="https://rootstack.com">Rootstack</a>.</p>

            <p>
                <a href="#">Ir al Inicio</a>
            </p>
        </footer>
    </main>




        <script src="{{asset('js/app.js')}}"></script>
    </body>
    <script>
        $("#btn_showUsers").click(function () {
            if($('#Terreno').val().length <1){
                alert('Debe ingresar un Tipo de Terreno, intente de nuevo');
                return;
            }
            //Creamos funcion para Capitalizar Nombres
            String.prototype.capitalize = function () {
                return this.replace(/(?:^|\s)\S/g, function (a) {
                    return a.toUpperCase();
                });
            };
            $('#buscando').show();
            /*Declaramos las variables a utilizar*/
            var i;
            var max_population=0;
            var planets=[];
            var parameter_terrain=$('#Terreno').val();
            var planet_name='';

            for (i=1;i<=7;i++){
                $.ajax({//Buscamos todas las paginas los planetas
                    url: 'https://swapi.co/api/planets/?page='+i,
                    dataType: 'json',
                    success: function (data) {
                        if(data['results'].length <1){
                            $('#buscando').html('No se encontraron coincidencias, intente con otro tipo de terreno');
                        }

                        //Obtenemos los planetas
                        data['results'].forEach(function (item){
                            $('#buscando').attr('style','display:none !important');
                            if (item['terrain'].toLocaleLowerCase().includes(parameter_terrain) ){
                                if (parseInt(item['population'])>max_population) {
                                    max_population=parseInt(item['population'])

                                    planet_name = item['name'];
                                    console.log(planet_name);
                                    $('#ships').html('');
                                    $('#ships').append(
                                            $('<div>')
                                                    .attr('class', 'col-md-6')
                                                    .append(
                                                    $('<div>')
                                                            .attr('class', 'card flex-md-row mb-4 box-shadow h-md-250')
                                                            .append(
                                                            $('<div>')
                                                                    .attr('class', 'card-body d-flex flex-column align-items-start')
                                                                    .append(
                                                                    $('<strong>')
                                                                            .attr('class', 'd-inline-block mb-2 text-primary')
                                                                            .append('Periodo de Rotacion: '+item['rotation_period'])
                                                            )
                                                                    .append(
                                                                    $('<h3>')
                                                                            .attr('class', 'mb-0')
                                                                            .append(
                                                                            $('<a>')
                                                                                    .attr('class', 'text-dark')
                                                                                    .append('Nombre: '+item['name'].capitalize() )
                                                                    )
                                                            )
                                                                    .append(
                                                                    $('<p>')
                                                                            .attr('class', 'card-text mb-auto')
                                                                            .append('Terreno: ' + item['terrain'])
                                                            )
                                                                    .append(
                                                                    $('<div>')
                                                                            .attr('class', 'mb-1 text-muted')
                                                                            .append('Poblacion: ' + item['population'] )
                                                            )
                                                                    .append(
                                                                    $('<strong>')
                                                                            .attr('class', 'd-inline-block mb-2 text-primary')
                                                                            .append('Periodo de Orbita: '+item['orbital_period'])
                                                            )
                                                    )

                                            )
                                    )

                                }



                            }
                        });

                    }
                });
            }


        });

    </script>

</html>
