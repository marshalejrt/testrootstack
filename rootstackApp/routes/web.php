<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/fetchorder', function () {
    return view('FetchOrder');
});

Route::get('/fetchfind', function () {
    return view('FetchFind');
});

Route::get('/fetchcount', function () {
    return view('FetchCount');
});


Route::get('/fastedship', function () {
    return view('FastedShip');
});

Route::get('/planetterrain', function () {
    return view('PlanetTerrain');
});
